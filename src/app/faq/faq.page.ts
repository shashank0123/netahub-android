import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import  { Router   } from '@angular/router';
import { Storage } from '@ionic/storage';
import { environment } from '../../environments/environment';
import { ToastController, Platform, AlertController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';

@Component({
  selector: 'app-faq',
  templateUrl: './faq.page.html',
  styleUrls: ['./faq.page.scss'],
})
export class FaqPage implements OnInit {
	baseUrl: string = environment.url;
	token: string = "";
	faqlist:any;

public user: object = {};
  profilepic: string = "";
  


  constructor(private http: HttpClient,
    private router: Router,
    private storage: Storage,
    private toastController: ToastController,
    private activatedRoute: ActivatedRoute,
    private _location:Location
    ) { }

  ngOnInit() {
    this.getSessionData();
  }
  // get session data
  getSessionData() {




    this.storage.get('user').then((name) => {
      this.user = name;
      this.profilepic =this.user[0].photo_path

      if (!name) {
        this.router.navigate(['/login']);
      }
    });

    

    // get user details




    this.storage.get('token').then((name) => {
      this.token = name;
      if (!name) {
        this.router.navigateByUrl('/login');
      }
      else{
      	 const completeUrl1 = `${this.baseUrl}/faq?token=${this.token}`;
      console.log(completeUrl1);
      this.http.get(completeUrl1).subscribe(data1 => {    
        if (data1) {
          this.faqlist = data1;

      console.log(this.faqlist);
          
        } else {
        }
      })
      }
    });
 
  
  }

  goBack(){
  this._location.back();
  }

}
