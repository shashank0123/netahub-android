import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ValidateNominationPage } from './validate-nomination.page';

const routes: Routes = [
  {
    path: '',
    component: ValidateNominationPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ValidateNominationPageRoutingModule {}
