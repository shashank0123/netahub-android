import { Component, OnInit, HostListener } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { environment } from "../../environments/environment";
import {
  ToastController,
  Platform,
  AlertController,
  LoadingController,
} from "@ionic/angular";
import { HttpClient } from "@angular/common/http";
import { Location } from "@angular/common";

@Component({
  selector: "app-validate-nomination",
  templateUrl: "./validate-nomination.page.html",
  styleUrls: ["./validate-nomination.page.scss"],
})
export class ValidateNominationPage implements OnInit {
  categories: any;
  user: any;
  profilepic: any;
  party_id: any;
  token: any;
  survey_id: any;
  cons_id: any;
  baseUrl: string = environment.url;

  constructor(
    private http: HttpClient,
    private router: Router,
    private storage: Storage,
    private toastController: ToastController,
    private activatedRoute: ActivatedRoute,
    private _location: Location,
    private loadingController: LoadingController
  ) {}

  ngOnInit() {
    this.token = this.activatedRoute.snapshot.queryParamMap.get("token");
    this.survey_id = this.activatedRoute.snapshot.queryParamMap.get(
      "survey_id"
    );
    this.cons_id = this.activatedRoute.snapshot.queryParamMap.get("cons_id");
    this.getSessionData();
  }
  getSessionData() {
    this.getcategorydata();
  }

  getcategorydata() {
    this.party_id = 0;
    this.storage.get("user").then((name) => {
      this.user = name;
      this.profilepic = this.user[0].photo_path;

      if (!name) {
        this.router.navigate(["/login"]);
      }
    });
    const completeUrl = `${this.baseUrl}/party?token=${this.token}`;
    this.http.get(completeUrl).subscribe((data) => {
      console.log("circle data: ", data);
      this.categories = data;
    });
  }
  // 'nominee_name', 'photo_path', 'nominee_desc'

  nominate() {
    console.log(this.party_id);
    const completeUrl = `${this.baseUrl}/insertnominee?token=${this.token}&survey_id=${this.survey_id}&party_id=${this.party_id}&cons_id=${this.cons_id}&nominee_name=&photo_path=&nominee_desc=`;
    this.http.get(completeUrl).subscribe((data) => {
      console.log("circle data: ", data);
      console.log("insert data ", data);
      var message = data[0].message;
      this.loadingController
        .create({
          message: message,
          duration: 2000,
        })
        .then((res) => {
          res.present().then(() => {
            this.router.navigateByUrl("tabs/tab1");
          });
        });
      // this.nomineeData = data;
    });
    // this.router.navigateByUrl('tabs/tab1');
  }

  goBack() {
    this._location.back();
  }
}
