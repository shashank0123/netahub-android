import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ValidateNominationPageRoutingModule } from './validate-nomination-routing.module';

import { ValidateNominationPage } from './validate-nomination.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ValidateNominationPageRoutingModule
  ],
  declarations: [ValidateNominationPage]
})
export class ValidateNominationPageModule {}
