import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ValidateNominationPage } from './validate-nomination.page';

describe('ValidateNominationPage', () => {
  let component: ValidateNominationPage;
  let fixture: ComponentFixture<ValidateNominationPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ValidateNominationPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ValidateNominationPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
