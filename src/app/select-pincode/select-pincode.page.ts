import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ToastController, Platform, AlertController, LoadingController } from '@ionic/angular';
import { finalize } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Location } from '@angular/common';

@Component({
  selector: 'app-select-pincode',
  templateUrl: './select-pincode.page.html',
  styleUrls: ['./select-pincode.page.scss'],
})
export class SelectPincodePage implements OnInit {
	 baseUrl: string = environment.url;
	 pincode: string = "";
	 token: string;
	 btntext: string = "Skip";
  authenticationState = new BehaviorSubject(false);
  public requestData: object = {};
  public responseData: object = { pincode: ''};




  public user: object = {};
  profilepic: string = "";
  





  constructor(private activatedRoute: ActivatedRoute, private router: Router,public loadingController: LoadingController,
    private http: HttpClient,
    private storage: Storage,
    private toastController: ToastController,
    private _location:Location
    ) { }

  ngOnInit() {
  }

  sendPostRequest(){

  	this.storage.get('token').then((name) => {
      this.token = name;
      console.log(this.requestData['pincode'])
      if (this.requestData['pincode'] != undefined){
        const completeUrl = `${this.baseUrl}/updatepincodeprofile?token=${this.token}&pincode=${this.requestData['pincode']}`;      
        this.http.get(completeUrl).subscribe(data => {
          if (data) {
            this.router.navigateByUrl('/select-country');
          } else {
            this.responseData['error_message'] = data['message'];
          }
        })
      }
      else{
            this.router.navigateByUrl('/select-country');

      }
      if (!name) {
        this.router.navigateByUrl('/login');
      }
    });
      
  }
   
  getcategorydata(){


  	this.storage.get('token').then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/party?token=${this.token}`;
    this.http.get(completeUrl).subscribe(data => {
      console.log('circle data: ', data);
      this.requestData['pincode'] = data
    })
      if (!name) {
        this.router.navigateByUrl('/login');
      }
    });
    
    
  }

  updatebtn(){
  	console.log(this.btntext)
  	if (this.requestData['pincode']!=""){
  		this.btntext = "Continue";
  	}
  	else
  		this.btntext = "Skip";

  }

goBack(){
  this._location.back();
  }

}
