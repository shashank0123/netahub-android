import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectPincodePageRoutingModule } from './select-pincode-routing.module';

import { SelectPincodePage } from './select-pincode.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectPincodePageRoutingModule
  ],
  declarations: [SelectPincodePage]
})
export class SelectPincodePageModule {}
