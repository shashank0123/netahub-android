import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectPincodePage } from './select-pincode.page';

const routes: Routes = [
  {
    path: '',
    component: SelectPincodePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectPincodePageRoutingModule {}
