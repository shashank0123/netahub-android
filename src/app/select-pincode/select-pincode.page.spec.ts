import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectPincodePage } from './select-pincode.page';

describe('SelectPincodePage', () => {
  let component: SelectPincodePage;
  let fixture: ComponentFixture<SelectPincodePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectPincodePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectPincodePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
