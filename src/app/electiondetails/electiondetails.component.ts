import { Component, OnInit } from '@angular/core';
import { 
ModalController, 
NavParams 
} from '@ionic/angular';


@Component({
  selector: 'app-electiondetails',
  templateUrl: './electiondetails.component.html',
  styleUrls: ['./electiondetails.component.scss'],
})
export class ElectiondetailsComponent implements OnInit {

  modelId: string;
  title: string;

  constructor(
    private modalController: ModalController,
    private navParams: NavParams
  ) { }

  ngOnInit() {
    this.modelId = this.navParams.data.data;
    this.title = this.navParams.data.title;
  }


}
