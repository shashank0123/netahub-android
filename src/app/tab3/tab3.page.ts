import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import  { Router   } from '@angular/router';
import { Storage } from '@ionic/storage';
import { environment } from '../../environments/environment';
import { ToastController, Platform, AlertController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { Location } from '@angular/common';

@Component({
  selector: 'app-tab3',
  templateUrl: 'tab3.page.html',
  styleUrls: ['tab3.page.scss']
})
export class Tab3Page implements OnInit{



	public user: object = {};
  baseUrl: string = environment.url;
  token: string = "";
  
  profilepic: string = "";



  constructor(
   private http: HttpClient,
    private router: Router,
    private storage: Storage,
    private toastController: ToastController,
    private activatedRoute: ActivatedRoute, 
    private _location:Location
  ) {}


 ngOnInit() {
    this.getSessionData();
  }
  // get session data


  getSessionData() {
    this.storage.get('user').then((name) => {
      this.user = name;
      this.profilepic =this.user[0].photo_path

      if (!name) {
        this.router.navigate(['/login']);
      }
    });

    

    // get user details
    

  }

goBack(){
  this._location.back();
  }


}
