import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectCollegePage } from './select-college.page';

const routes: Routes = [
  {
    path: '',
    component: SelectCollegePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectCollegePageRoutingModule {}
