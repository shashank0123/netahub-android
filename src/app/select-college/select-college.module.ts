import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectCollegePageRoutingModule } from './select-college-routing.module';

import { SelectCollegePage } from './select-college.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectCollegePageRoutingModule
  ],
  declarations: [SelectCollegePage]
})
export class SelectCollegePageModule {}
