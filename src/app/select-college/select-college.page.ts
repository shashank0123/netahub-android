import { ActivatedRoute, Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { Storage } from "@ionic/storage";
import {
  ToastController,
  Platform,
  AlertController,
  LoadingController,
} from "@ionic/angular";
import { finalize } from "rxjs/operators";
import { environment } from "../../environments/environment";
import { tap, catchError } from "rxjs/operators";
import { BehaviorSubject } from "rxjs";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Location } from "@angular/common";

@Component({
  selector: "app-select-college",
  templateUrl: "./select-college.page.html",
  styleUrls: ["./select-college.page.scss"],
})
export class SelectCollegePage implements OnInit {
  baseUrl: string = environment.url;
  college: any;
  token: string;
  new_element: string = "0";
  state_id: string;
  btntext: string = "Submit";
  authenticationState = new BehaviorSubject(false);
  public requestData: object = {};
  public responseData: object = { state: "", userstate: "" };

  public user: object = {};
  profilepic: string = "";

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public loadingController: LoadingController,
    private http: HttpClient,
    private storage: Storage,
    private toastController: ToastController,
    private _location: Location
  ) {}

  ngOnInit() {
    this.state_id = this.activatedRoute.snapshot.queryParamMap.get("state_id");
    this.getcategorydata();
  }

  sendPostRequest() {
    this.storage.get("token").then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/updateuserprofile?token=${this.token}&college_id=${this.requestData["usercollege"]}&state_id=${this.state_id}&college_name=${this.requestData["usercollegename"]}`;
      this.http.get(completeUrl).subscribe((data) => {
        if (data) {
          this.loadingController
            .create({
              message: "Data Saved Successfully",
              duration: 2000,
            })
            .then((res) => {
              res.present().then(() => {
                this.router.navigateByUrl("profile");
              });
            });
        } else {
          this.responseData["error_message"] = data[0].message;
        }
      });
      if (!name) {
        this.router.navigateByUrl("/login");
      }
    });
  }

  getcategorydata() {
    this.storage.get("token").then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/college?token=${this.token}&state_id=${this.state_id}`;
      this.http.get(completeUrl).subscribe((data) => {
        console.log("circle data: ", data);
        this.college = data;
        const completeUrl1 = `${this.baseUrl}/usercollege?token=${this.token}`;
        this.http.get(completeUrl1).subscribe((data1) => {
          console.log("circle data: ", data1);
          if (data1[0] != undefined && data1[0].college_id != undefined)
            this.requestData["usercollege"] = data1[0].college_id;
          console.log(this.requestData["usercountry"]);
        });
      });

      if (!name) {
        this.router.navigateByUrl("/login");
      }
    });
  }

  updatebtn() {
    console.log(this.btntext);
    if (this.requestData["usercollege"] != "") {
      this.btntext = "Submit";
    } else this.btntext = "Skip";
  }

  goBack() {
    this._location.back();
  }
}
