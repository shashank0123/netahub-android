import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectCollegePage } from './select-college.page';

describe('SelectCollegePage', () => {
  let component: SelectCollegePage;
  let fixture: ComponentFixture<SelectCollegePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectCollegePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectCollegePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
