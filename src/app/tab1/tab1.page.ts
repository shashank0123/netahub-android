import { Component, OnInit, HostListener } from "@angular/core";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { environment } from "../../environments/environment";
import { HttpClient } from "@angular/common/http";
import { PopoverController } from "@ionic/angular";
import { SettingsComponent } from "../setting/setting.component";
import { ElectiondetailsComponent } from "../electiondetails/electiondetails.component";
import { Capacitor, Plugins } from "@capacitor/core";
import { Globalization } from "@ionic-native/globalization/ngx";
import { TranslateService } from "@ngx-translate/core";

const { Share } = Plugins;

@Component({
  selector: "app-tab1",
  templateUrl: "tab1.page.html",
  styleUrls: ["tab1.page.scss"],
})
export class Tab1Page {
  constructor(
    private http: HttpClient,
    private router: Router,
    private storage: Storage,
    private popoverController: PopoverController,
    private globalization: Globalization,
    private _translate: TranslateService,
    // public socialSharing: SocialSharing
  ) {}

  public user: object = {};
  baseUrl: string = environment.url;
  token = "";
  user_photo = "";
  constituency = "";
  electiondata: any = "";
  subscription: any;
  platform: any = Capacitor.platform;
  public election: string;
  public elections: string;
  public nominate: string;
  public name: string;
  public language: string;

  ngOnInit() {
    // this.getSessionData();
    this.getSessionData();
  }

  ionViewWillEnter() {}
  ionViewDidEnter() {
    this.getSessionData();
    this.getDeviceLanguage();
  }

  _initialiseTranslation(): void {
    this._translate.get("nominate").subscribe((res: string) => {
      this.nominate = res;
    });

    this._translate
      .get("elections", { value: "John" })
      .subscribe((res: string) => {
        this.elections = res;
      });
    this._translate
      .get("election", { name_value: "Marissa Mayer" })
      .subscribe((res: string) => {
        this.election = res;
      });
  }

  public changeLanguage(): void {
    this._translateLanguage();
  }

  _translateLanguage(): void {
    this._translate.use(this.language);
    this._initialiseTranslation();
  }

  _initTranslate(language) {
    // Set the default language for translation strings, and the current language.
    this._translate.setDefaultLang("en");
    if (language) {
      this.language = language;
    } else {
      // Set your language here
      this.language = "en";
    }
    this._translateLanguage();
  }

  getDeviceLanguage() {
    if (window.Intl && typeof window.Intl === "object") {
      this._initTranslate(navigator.language);
    } else {
      this.globalization
        .getPreferredLanguage()
        .then((res) => {
          this._initTranslate(res.value);
        })
        .catch((e) => {
          console.log(e);
        });
    }
  }

  ionViewWillLeave() {
    this.subscription.unsubscribe();
  }

  // get session data
  getSessionData() {
    this.storage.get("user").then((name) => {
      this.user = name;
      // console.log(this.user);

      if (!name) {
        this.router.navigateByUrl("/login");
      }
    });

    this.storage.get("token").then((name) => {
      this.token = name;
      // console.log(name)

      this.getElectionData();
      if (!name) {
        this.router.navigateByUrl("/login");
      }
    });

    this.storage.get("user_photo").then((name) => {
      this.user_photo = name;
      // console.log(name)

      this.getElectionData();
      if (!name) {
        this.router.navigateByUrl("/login");
      }
    });

    this.storage.get("constituency").then((name) => {
      this.constituency = name;
    });

    // get user details
  }

  doRefresh(event) {
    // console.log('Begin async operation');
    setTimeout(() => {
      // console.log('Async operation has ended');
      event.target.complete();
    }, 2000);

    this.getSessionData();
  }

  async showDetails(ev: any, data) {
    const popover = await this.popoverController.create({
      component: ElectiondetailsComponent,
      componentProps: {
        data: ev,
        title: data,
      },
      event: ev,
      animated: true,
      showBackdrop: true,
    });
    return await popover.present();
  }
  // get Circle
  getElectionData() {
    const completeUrl = `${this.baseUrl}/election?token=${this.token}`;
    // console.log(completeUrl);
    this.http.get(completeUrl).subscribe((data) => {
      console.log("circle data: ", data);
      if (data[0].message != undefined && data[0].message == "Invalid Token") {
        this.router.navigateByUrl("/login");
      } else {
        this.electiondata = data;
      }
    });
  }

  // redirectOn
  redirectOn(route) {
    this.router.navigate([route]);
  }

  getLogin() {
    this.router.navigateByUrl("nomini/1");
  }

  getNominee(id, is_active, is_eligible, cons_id) {
    console.log(this.constituency);
    if (this.constituency == "0") {
      this.router.navigateByUrl("update-consituency");
    } else {
      if (is_active == 1) {
        if (is_eligible == 1) {
          this.router.navigateByUrl(
            "nomini/?survey_id=" + id + "&cons_id=" + cons_id
          );
        } else {
          this.router.navigateByUrl("profile");
        }
      } else {
        // console.log('it cannot be clicked.');
      }
    }
  }

  nominationParty(election) {
    this.router.navigateByUrl(
      "validate-nomination?survey_id=" +
        election.survey_id +
        "&token=" +
        this.token +
        "&cons_id=" +
        election.cons_id
    );
  }

  nominationOther(election) {
    this.router.navigateByUrl(
      "nominate-other?survey_id=" +
        election.survey_id +
        "&token=" +
        this.token +
        "&cons_id=" +
        election.cons_id
    );
  }

  async shareApp(title){
    let shareRet = await Share.share({
      text: title,
    });
  }

  mapuser(election) {
    const completeUrl = `${this.baseUrl}/updatesurvey?token=${this.token}&survey_id=${election.survey_id}&election_id=${election.election_id}`;
    // console.log(completeUrl);
    this.http.get(completeUrl).subscribe((data) => {
      // console.log('circle data: ', data);
      if (data[0].message != undefined && data[0].message == "Invalid Token") {
        this.router.navigateByUrl("/login");
      } else {
        this.router.navigateByUrl("/");
        // this.electiondata = data;
      }
    });
    this.getSessionData();
  }

  createsurvey(election) {
    this.router.navigateByUrl(
      "/survey-name?token=" +
        this.token +
        "&election_id=" +
        election.election_id +
        "&election_name=" +
        election.election_name
    );
  }

  async settingsPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: SettingsComponent,
      event: ev,
      componentProps: { page: "Home" },
      cssClass: "popover_class",
    });
  }
}
