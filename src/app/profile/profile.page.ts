import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { environment } from "../../environments/environment";
import { ToastController, Platform, AlertController } from "@ionic/angular";
import { HttpClient } from "@angular/common/http";
import { PhotoService } from "../services/photo.service";
import {
  Plugins,
  CameraResultType,
  Capacitor,
  FilesystemDirectory,
  CameraPhoto,
  CameraSource,
} from "@capacitor/core";
import { Location } from "@angular/common";

const { Camera, Filesystem } = Plugins;
const options = {
  resultType: CameraResultType.Uri,
};
@Component({
  selector: "app-profile",
  templateUrl: "./profile.page.html",
  styleUrls: ["./profile.page.scss"],
})
export class ProfilePage implements OnInit {
  profilepic: string = "";
  user_name: string = "";
  display_name: string = "";
  district_name: string = "";
  gender: string = "";
  public user: object = {};
  baseUrl: string = environment.url;
  token: string = "";
  election_id: string = "";
  email_id: string = "";
  dateofbirth: string = "";
  mobile_no: string = "";
  qualification: string = "";
  occupation: string = "";
  village_name: string = "";
  pincode: string = "";
  college_name: string = "";
  society_name: string = "";
  state_id: string = "";
  district_id: string = "";
  nomineeData: any = "";
  selectedImage: any = "";
  customerror: any = "";

  constructor(
    private http: HttpClient,
    private router: Router,
    private storage: Storage,
    private toastController: ToastController,
    private activatedRoute: ActivatedRoute,
    public photoService: PhotoService,
    private _location: Location
  ) {}

  ngOnInit() {
    this.getSessionData();
  }

  ionViewWillEnter() {
    this.getSessionData();
  }

  public async addNewToGallery(token) {
    // Take a photo
    const capturedPhoto = await Plugins.Camera.getPhoto({
      resultType: CameraResultType.Base64, // file-based data; provides best performance
      source: CameraSource.Photos, // automatically take a new photo with the camera
      quality: 100, // highest quality (0 to 100)
    }).then((image) => {
      this.selectedImage = image.base64String; // VAR TO DISPLAY IN HTML
    });

    // console.log(response.value);
    const formData = new FormData();
    formData.append("filedata", this.selectedImage);
    formData.append("token", this.token);
    const completeUrl = `${this.baseUrl}/updateuserphoto`;
    this.http.post(completeUrl, formData).subscribe((data) => {
      console.log("circle data: ", JSON.stringify(data));
      this.customerror = this.selectedImage;
      // this.nomineeData = data;
      this.getSessionData();
    });

    console.log("hello");

    // Save the picture and add it to photo collection
    // const savedImageFile = await this.savePicture(capturedPhoto);
    // this.photos.unshift(savedImageFile);
  }

  // private async uploadTus(webPath: string) {

  //   const blob = await fetch(webPath).then(r => r.blob());

  //   const formData = new FormData();
  //   formData.append('file', blob, `file-profilepic.jpg`);
  //   this.http.post<boolean>(`${this.baseUrl}/updateuserphoto?token=${this.token}`, formData)

  //     .subscribe();
  // }
  // get session data
  getSessionData() {
    this.storage.get("token").then((name) => {
      this.token = name;
      if (!name) {
        this.router.navigateByUrl("/login");
      }
    });

    this.storage.get("user").then((name) => {
      this.user = name;
      // console.log(this.user[0]);
      const completeUrl1 = `${this.baseUrl}/user?token=${this.token}&login_id=${this.user[0].user_name}`;
      this.http.get(completeUrl1).subscribe((data1) => {
        if (data1) {
          console.log(data1[0]);
          this.user = data1[0];
          this.storage.set("user", data1);
          this.profilepic = data1[0].photo_path;
          this.user_name = data1[0].user_name;
          this.district_name = data1[0].district_name;
          this.display_name = data1[0].display_name;
          this.gender = data1[0].gender;
          this.dateofbirth = data1[0].dateofbirth;
          this.email_id = data1[0].email_id;
          this.mobile_no = data1[0].mobile_no;
          this.village_name =
            data1[0].village_name +
            ", " +
            data1[0].district_name +
            " - " +
            data1[0].pincode;
          this.qualification = data1[0].qualification;
          this.occupation = data1[0].occupation;
          this.college_name = data1[0].college_name;
          this.society_name = data1[0].society_name;
          this.state_id = data1[0].state_id;
          this.district_id = data1[0].district_id;
        } else {
        }
      });

      if (!name) {
        this.router.navigateByUrl("/login");
      }
    });
  }

  // async takePicture() {
  //   const image = await Camera.getPhoto({
  //     quality: 90,
  //     allowEditing: true,
  //     resultType: CameraResultType.Uri
  //   });
  //   // image.webPath will contain a path that can be set as an image src.
  //   // You can access the original file using image.path, which can be
  //   // passed to the Filesystem API to read the raw data of the image,
  //   // if desired (or pass resultType: CameraResultType.Base64 to getPhoto)
  //   var imageUrl = image.webPath;
  //   // Can be set to the src of an image now
  //   // imageElement.src = imageUrl;
  // }

  // saveimage(){
  //   Plugins.Camera.getPhoto({
  //       quality: 100,
  //       source: CameraSource.Prompt,
  //       correctOrientation: true,
  //       allowEditing: false,
  //       resultType: CameraResultType.Base64
  //   })
  //    .then(image => {
  //         this.selectedImage = image.base64Data; // VAR TO DISPLAY IN HTML
  //     })
  // }

  editcollege() {
    this.router.navigateByUrl("/select-college?state_id=" + this.state_id);
  }

  editsociety() {
    this.router.navigateByUrl(
      "/select-society?district_id=" + this.district_id
    );
  }

  goBack() {
    this._location.back();
  }
}
