import { ActivatedRoute, Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { Storage } from "@ionic/storage";
import {
  ToastController,
  Platform,
  AlertController,
  LoadingController,
} from "@ionic/angular";
import { finalize } from "rxjs/operators";
import { environment } from "../../environments/environment";
import { tap, catchError } from "rxjs/operators";
import { BehaviorSubject } from "rxjs";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Location } from "@angular/common";

@Component({
  selector: "app-select-ward",
  templateUrl: "./select-ward.page.html",
  styleUrls: ["./select-ward.page.scss"],
})
export class SelectWardPage implements OnInit {
  baseUrl: string = environment.url;
  ward: any;
  token: string;
  village_id: string;
  btntext: string = "Submit";
  authenticationState = new BehaviorSubject(false);
  public requestData: object = {};
  public responseData: object = { state: "", userstate: "" };

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public loadingController: LoadingController,
    private http: HttpClient,
    private storage: Storage,
    private toastController: ToastController,
    private _location: Location
  ) {}

  ngOnInit() {
    this.village_id = this.activatedRoute.snapshot.queryParamMap.get(
      "village_id"
    );
    this.getcategorydata();
  }

  sendPostRequest() {
    this.storage.get("token").then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/updateuserprofile?token=${this.token}&ward_id=${this.requestData["userward"]}&village_id=${this.village_id}&ward_name=${this.requestData["userwardname"]}`;
      this.http.get(completeUrl).subscribe((data) => {
        if (data) {
          this.loadingController
            .create({
              message: "Data Saved Successfully",
              duration: 2000,
            })
            .then((res) => {
              res.present().then(() => {
                this.router.navigateByUrl("profile");
              });
            });
        } else {
          this.responseData["error_message"] = data[0].message;
        }
      });
      if (!name) {
        this.router.navigateByUrl("/login");
      }
    });
  }

  getcategorydata() {
    this.storage.get("token").then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/ward?token=${this.token}&village_id=${this.village_id}`;
      this.http.get(completeUrl).subscribe((data) => {
        console.log("circle data: ", data);
        if (data[0] != undefined) this.ward = data;
        else this.requestData["userward"] = 0;
        const completeUrl1 = `${this.baseUrl}/userward?token=${this.token}`;
        this.http.get(completeUrl1).subscribe((data1) => {
          console.log("circle data user: ", data1);
          if (data1[0] != undefined && data1[0].ward_id != undefined)
            this.requestData["userward"] = data1[0].ward_id;
          console.log(this.requestData["userward"]);
        });
      });

      if (!name) {
        this.router.navigateByUrl("/login");
      }
    });
  }

  updatebtn() {
    console.log(this.btntext);
    if (this.requestData["userward"] != "") {
      this.btntext = "Submit";
    } else this.btntext = "Skip";
  }

  goBack() {
    this._location.back();
  }
}
