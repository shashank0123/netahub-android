import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectWardPage } from './select-ward.page';

describe('SelectWardPage', () => {
  let component: SelectWardPage;
  let fixture: ComponentFixture<SelectWardPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectWardPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectWardPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
