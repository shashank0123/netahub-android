import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectWardPageRoutingModule } from './select-ward-routing.module';

import { SelectWardPage } from './select-ward.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectWardPageRoutingModule
  ],
  declarations: [SelectWardPage]
})
export class SelectWardPageModule {}
