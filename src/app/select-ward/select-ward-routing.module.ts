import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectWardPage } from './select-ward.page';

const routes: Routes = [
  {
    path: '',
    component: SelectWardPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectWardPageRoutingModule {}
