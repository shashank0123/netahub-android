import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';

const routes: Routes = [
  {
    path: '',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule)
  },
    // { path: '', redirectTo: 'home', pathMatch: 'full' },
  { path: '', redirectTo: 'tablinks', pathMatch: 'full' },
  { path: 'home', loadChildren: () => import('./home/home.module').then( m => m.HomePageModule)},
  {
    path: 'login',
    loadChildren: () => import('./login/login.module').then( m => m.LoginPageModule)
  },
  {
    path: 'register',
    loadChildren: () => import('./register/register.module').then( m => m.RegisterPageModule)
  },
  {
    path: 'otp',
    loadChildren: () => import('./otp/otp.module').then( m => m.OtpPageModule)
  },
  {
    path: 'nomini/:election_id',
    loadChildren: () => import('./nomini/nomini.module').then( m => m.NominiPageModule)
  },
  {
    path: 'profile',
    loadChildren: () => import('./profile/profile.module').then( m => m.ProfilePageModule)
  },
  {
    path: 'nomini-detail/:nomini_id',
    loadChildren: () => import('./nomini-detail/nomini-detail.module').then( m => m.NominiDetailPageModule),
  },
  {
    path: 'voting-history',
    loadChildren: () => import('./voting-history/voting-history.module').then( m => m.VotingHistoryPageModule)
  },
  {
    path: 'voting-detail',
    loadChildren: () => import('./voting-detail/voting-detail.module').then( m => m.VotingDetailPageModule),
  },
  {
    path: 'forgotpassword',
    loadChildren: () => import('./forgotpassword/forgotpassword.module').then( m => m.ForgotpasswordPageModule)
  },
  {
    path: 'settings',
    loadChildren: () => import('./settings/settings.module').then( m => m.SettingsPageModule)
  },
  {
    path: 'tablinks',
    loadChildren: () => import('./tablinks/tablinks.module').then( m => m.TablinksPageModule)
  },
  {
    path: 'validate-nomination',
    loadChildren: () => import('./validate-nomination/validate-nomination.module').then( m => m.ValidateNominationPageModule)
  },
  {
    path: 'feedbackpage',
    loadChildren: () => import('./feedbackpage/feedbackpage.module').then( m => m.FeedbackpagePageModule)
  },
  {
    path: 'faq',
    loadChildren: () => import('./faq/faq.module').then( m => m.FaqPageModule)
  },
  {
    path: 'select-pincode',
    loadChildren: () => import('./select-pincode/select-pincode.module').then( m => m.SelectPincodePageModule)
  },
  {
    path: 'select-country',
    loadChildren: () => import('./select-country/select-country.module').then( m => m.SelectCountryPageModule)
  },
  {
    path: 'select-state',
    loadChildren: () => import('./select-state/select-state.module').then( m => m.SelectStatePageModule)
  },
  {
    path: 'select-district',
    loadChildren: () => import('./select-district/select-district.module').then( m => m.SelectDistrictPageModule)
  },
  {
    path: 'select-tehsil',
    loadChildren: () => import('./select-tehsil/select-tehsil.module').then( m => m.SelectTehsilPageModule)
  },
  {
    path: 'select-block',
    loadChildren: () => import('./select-block/select-block.module').then( m => m.SelectBlockPageModule)
  },
  {
    path: 'select-village',
    loadChildren: () => import('./select-village/select-village.module').then( m => m.SelectVillagePageModule)
  },
  {
    path: 'select-ward',
    loadChildren: () => import('./select-ward/select-ward.module').then( m => m.SelectWardPageModule)
  },
  {
    path: 'select-society',
    loadChildren: () => import('./select-society/select-society.module').then( m => m.SelectSocietyPageModule)
  },
  {
    path: 'select-college',
    loadChildren: () => import('./select-college/select-college.module').then( m => m.SelectCollegePageModule)
  },
  {
    path: 'survey-name',
    loadChildren: () => import('./survey-name/survey-name.module').then( m => m.SurveyNamePageModule)
  },
  {
    path: 'update-consituency',
    loadChildren: () => import('./update-consituency/update-consituency.module').then( m => m.UpdateConsituencyPageModule)
  },
  {
    path: 'nominate-other',
    loadChildren: () => import('./nominate-other/nominate-other.module').then( m => m.NominateOtherPageModule)
  },
  {
    path: 'gender-analysis',
    loadChildren: () => import('./gender-analysis/gender-analysis.module').then( m => m.GenderAnalysisPageModule)
  },
  {
    path: 'age-analysis',
    loadChildren: () => import('./age-analysis/age-analysis.module').then( m => m.AgeAnalysisPageModule)
  },
  {
    path: 'location-analysis',
    loadChildren: () => import('./location-analysis/location-analysis.module').then( m => m.LocationAnalysisPageModule)
  },
  {
    path: 'profession-analysis',
    loadChildren: () => import('./profession-analysis/profession-analysis.module').then( m => m.ProfessionAnalysisPageModule)
  },
];
@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
  
