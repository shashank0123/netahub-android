import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NominiPageRoutingModule } from './nomini-routing.module';

import { NominiPage } from './nomini.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NominiPageRoutingModule
  ],
  declarations: [NominiPage]
})
export class NominiPageModule {}
