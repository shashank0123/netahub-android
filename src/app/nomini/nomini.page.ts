import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { LoadingController } from "@ionic/angular";
import { Storage } from "@ionic/storage";
import { environment } from "../../environments/environment";
import { ToastController, Platform, AlertController } from "@ionic/angular";
import { Capacitor, Plugins } from "@capacitor/core";

import { HttpClient } from "@angular/common/http";

const { Share } = Plugins;
@Component({
  selector: "app-nomini",
  templateUrl: "./nomini.page.html",
  styleUrls: ["./nomini.page.scss"],
})
export class NominiPage implements OnInit {
  public user: object = {};
  baseUrl: string = environment.url;
  token: string = "";
  survey_id: string = "";
  cons_id: string = "";
  nomineeData: any = "";
  profilepic: string = "";

  constructor(
    private http: HttpClient,
    private router: Router,
    private storage: Storage,
    private toastController: ToastController,
    private activatedRoute: ActivatedRoute,
    private loadingController: LoadingController
  ) {}

  ngOnInit() {
    this.getSessionData();
  }
  // get session data
  getSessionData() {
    this.storage.get("user").then((name) => {
      this.user = name;
      this.profilepic = this.user[0].photo_path;

      if (!name) {
        this.router.navigate(["/login"]);
      }
    });

    this.storage.get("token").then((name) => {
      this.token = name;
      // console.log(this.token)

      this.getNomineeData();
      if (!name) {
        this.router.navigate(["/login"]);
      }
    });

    // get user details
  }

  // get Circle
  getNomineeData() {
    this.survey_id = this.activatedRoute.snapshot.queryParamMap.get(
      "survey_id"
    );
    this.cons_id = this.activatedRoute.snapshot.queryParamMap.get("cons_id");
    const completeUrl = `${this.baseUrl}/nominee?token=${this.token}&survey_id=${this.survey_id}&cons_id=${this.cons_id}`;
    this.http.get(completeUrl).subscribe((data) => {
      console.log("circle data: ", data);
      this.nomineeData = data;
    });
  }

  getNomineeDetails(nominee) {
    this.storage.set("nominee", nominee);
    this.router.navigateByUrl("nomini-detail/1?cons_id=" + this.cons_id);
  }

  castVote(nominee_id, survey_id) {
    const completeUrl = `${this.baseUrl}/insertsurveypoll?token=${this.token}&survey_id=${survey_id}&nominee_id=${nominee_id}&cons_id=${this.cons_id}`;
    this.http.get(completeUrl).subscribe((data) => {
      console.log("circle data: ", data);
      // this.nomineeData = data;
      const completeUrl1 = `${this.baseUrl}/nominee?token=${this.token}&survey_id=${this.survey_id}&cons_id=${this.cons_id}`;
      this.http.get(completeUrl1).subscribe((data) => {
        console.log("circle data: ", data);
        this.loadingController
          .create({
            message: "Voted successfully",
            duration: 2000,
          })
          .then((res) => {
            res.present();
            this.nomineeData = data;
          });
      });
    });
    // this.router.navigateByUrl('nomini/?survey_id=' + survey_id);
  }
  async shareApp(title){
    let shareRet = await Share.share({
      text: title,
    });
  }

  getProfile() {
    this.router.navigateByUrl("profile");
  }
}
