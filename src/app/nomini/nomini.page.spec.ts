import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NominiPage } from './nomini.page';

describe('NominiPage', () => {
  let component: NominiPage;
  let fixture: ComponentFixture<NominiPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NominiPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NominiPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
