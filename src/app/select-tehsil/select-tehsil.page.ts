import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ToastController, Platform, AlertController, LoadingController } from '@ionic/angular';
import { finalize } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Location } from '@angular/common';

@Component({
  selector: 'app-select-tehsil',
  templateUrl: './select-tehsil.page.html',
  styleUrls: ['./select-tehsil.page.scss'],
})
export class SelectTehsilPage implements OnInit {
	baseUrl: string = environment.url;
	 tehsil: any;
	 token: string;
	 district_id: string;
	 btntext: string = "Continue";
  authenticationState = new BehaviorSubject(false);
  public requestData: object = {};
  public responseData: object = { state: '', userstate: ''};

  constructor(private activatedRoute: ActivatedRoute, private router: Router,public loadingController: LoadingController,
    private http: HttpClient,
    private storage: Storage,
    private toastController: ToastController,
    private _location:Location) { }

  ngOnInit() {
  	this.district_id = this.activatedRoute.snapshot.queryParamMap.get("district_id")
    this.getcategorydata();
  }

  sendPostRequest(){

  	this.storage.get('token').then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/updateuserprofile?token=${this.token}&tehsil_id=${this.requestData['usertehsil']}&district_id=${this.district_id}&tehsil_name=${this.requestData['usertehsilname']}`;      
      this.http.get(completeUrl).subscribe(data => {
        if (data) {
          this.router.navigateByUrl('/select-block?tehsil_id='+this.requestData['usertehsil']);
        } else {
          this.responseData['error_message'] = data['message'];
        }
      })
      if (!name) {
        this.router.navigateByUrl('/login');
      }
    });
      
  }
   
  getcategorydata(){


  	this.storage.get('token').then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/tehsil?token=${this.token}&district_id=${this.district_id}`;
	    this.http.get(completeUrl).subscribe(data => {
	      console.log('circle data: ', data);
	      this.tehsil = data
	      const completeUrl1 = `${this.baseUrl}/usertehsil?token=${this.token}`;
	    this.http.get(completeUrl1).subscribe(data1 => {
	      console.log('circle data: ', data1);
	      if (data1[0] != undefined && data1[0].tehsil_id != undefined)
	      this.requestData['usertehsil'] = data1[0].tehsil_id
	  console.log(this.requestData['usertehsil'])
	    })
	    })

	    
      if (!name) {
        this.router.navigateByUrl('/login');
      }
    });
    
    
  }

  updatebtn(){
  	console.log(this.btntext)
  	if (this.requestData['userdistrict']!=""){
  		this.btntext = "Continue";
  	}
  	else
  		this.btntext = "Skip";

  }

goBack(){
  this._location.back();
  }

}
