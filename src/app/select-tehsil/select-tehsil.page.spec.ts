import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectTehsilPage } from './select-tehsil.page';

describe('SelectTehsilPage', () => {
  let component: SelectTehsilPage;
  let fixture: ComponentFixture<SelectTehsilPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectTehsilPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectTehsilPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
