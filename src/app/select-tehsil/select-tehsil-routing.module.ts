import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectTehsilPage } from './select-tehsil.page';

const routes: Routes = [
  {
    path: '',
    component: SelectTehsilPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectTehsilPageRoutingModule {}
