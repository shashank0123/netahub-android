import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectTehsilPageRoutingModule } from './select-tehsil-routing.module';

import { SelectTehsilPage } from './select-tehsil.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectTehsilPageRoutingModule
  ],
  declarations: [SelectTehsilPage]
})
export class SelectTehsilPageModule {}
