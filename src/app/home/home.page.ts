import { Component, OnInit, HostListener  } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import  { Router   } from '@angular/router';
import { Storage } from '@ionic/storage';
import { environment } from '../../environments/environment';
import { ToastController, Platform, AlertController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { PopoverController } from '@ionic/angular';
import { SettingsComponent } from '../setting/setting.component';
import { ElectiondetailsComponent } from '../electiondetails/electiondetails.component';


@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})

export class HomePage implements OnInit {
  constructor(
    private http: HttpClient,
    private router: Router,
    private storage: Storage,
    private toastController: ToastController,
    private activatedRoute: ActivatedRoute,
    private popoverController: PopoverController
    ) {}
  public user: object = {};
  baseUrl: string = environment.url;
  token = '';
  electiondata: any = '';


  ngOnInit() {
    this.getSessionData();
  }
  // get session data
  getSessionData() {
    this.storage.get('user').then((name) => {
      this.user = name;
      console.log(this.user);

      if (!name) {
        this.router.navigateByUrl('/login');
      }
    });

    this.storage.get('token').then((name) => {
      this.token = name;
      // console.log(this.token)

      this.getElectionData();
      if (!name) {
        this.router.navigateByUrl('/login');
      }
    });

    // get user details
  }


async showDetails(ev: any, data) {
  const popover = await this.popoverController.create({
    component: ElectiondetailsComponent,
      event: ev,
      animated: true,
      showBackdrop: true
    });
  return await popover.present();
  }
  // get Circle
  getElectionData() {
    const completeUrl = `${this.baseUrl}/election?token=${this.token}`;
    console.log(completeUrl);
    this.http.get(completeUrl).subscribe(data => {
      console.log('circle data: ', data);
      if (data[0].message != undefined && data[0].message == 'Invalid Token') {
        this.router.navigateByUrl('/login');
      } else {
      this.electiondata = data;
      }
    });
  }

  // redirectOn
  redirectOn(route) {
    this.router.navigate([route]);
  }

  getLogin() {
    this.router.navigateByUrl('nomini/1');
  }

  getNominee(id) {
    if (this.electiondata[id].is_active == 1) {
      if (this.electiondata[id].is_eligible == 1){
        this.router.navigateByUrl('nomini/?survey_id=' + id);
      } else{
        this.router.navigateByUrl('profile');

      }
    } else {
      console.log('it cannot be clicked.');
    }
    // this.router.navigate['nomini/1'];
  }
  logout() {
    console.log(this.storage.get('token'));
    this.router.navigateByUrl('/login');
  }


  async settingsPopover(ev: any) {
    const popover = await this.popoverController.create({
      component: SettingsComponent,
      event: ev,
      componentProps: { page: 'Home' },
      cssClass: 'popover_class',
    });


  }

}




