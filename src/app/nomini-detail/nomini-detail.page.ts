import { Component, OnInit } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { environment } from "../../environments/environment";
import {
  ToastController,
  Platform,
  AlertController,
  LoadingController,
} from "@ionic/angular";
import { HttpClient } from "@angular/common/http";
import { Location } from "@angular/common";

@Component({
  selector: "app-nomini-detail",
  templateUrl: "./nomini-detail.page.html",
  styleUrls: ["./nomini-detail.page.scss"],
})
export class NominiDetailPage implements OnInit {
  public user: object = {};
  profilepic: string = "";
  user_pic: string = "";
  nominee_name: string = "";
  party_name: string = "";
  user_information: string = "";
  display_name: string = "";
  district_name: string = "";
  gender: string = "";
  baseUrl: string = environment.url;
  token: string = "";
  election_id: string = "";
  email_id: string = "";
  survey_id: string = "";
  cons_id: string = "";
  nominee_id: string = "";
  voted: string = "";
  nomineeData: any = "";

  constructor(
    private http: HttpClient,
    private router: Router,
    private storage: Storage,
    private toastController: ToastController,
    private activatedRoute: ActivatedRoute,
    private loadingController: LoadingController,
    private _location: Location
  ) {}

  ngOnInit() {
    this.getSessionData();
    this.cons_id = this.activatedRoute.snapshot.queryParamMap.get("cons_id");
  }
  // get session data
  getSessionData() {
    this.storage.get("nominee").then((name) => {
      this.user = name;
      console.log(this.user);
      this.profilepic = this.user["photo_path"];
      this.nominee_name = this.user["nominee_name"];
      this.party_name = this.user["party_name"];
      this.user_information = this.user["user_information"];
      this.survey_id = this.user["survey_id"];
      this.nominee_id = this.user["nominee_id"];
      this.cons_id = this.user["cons_id"];
      this.voted = this.user["voted"];
      console.log(this.voted);
      this.storage.get("token").then((name) => {
        this.token = name;
        console.log(this.token);

        if (!name) {
          this.router.navigate(["/login"]);
        }
      });
    });
    // get user details

    this.storage.get("user").then((name) => {
      this.user = name;
      this.user_pic = this.user[0].photo_path;

      if (!name) {
        this.router.navigate(["/login"]);
      }
    });
  }

  castVote(nominee_id, survey_id) {
    this.cons_id = this.activatedRoute.snapshot.queryParamMap.get("cons_id");
    const completeUrl = `${this.baseUrl}/insertsurveypoll?token=${this.token}&survey_id=${survey_id}&nominee_id=${nominee_id}&cons_id=${this.cons_id}`;
    this.http.get(completeUrl).subscribe((data) => {
      console.log("circle data: ", data);
      if (data[0].message == "Voted Successfully")
        this.loadingController
          .create({
            message: "Voted successfully",
            duration: 2000,
          })
          .then((res) => {
            res.present();
          });
      this.voted = "1";
    });
    // this.router.navigateByUrl('nomini/?survey_id=' + survey_id);
  }

  getNomini() {
    this.router.navigate(["nomini/1"]);
  }

  getProfile() {
    this.router.navigate(["profile"]);
  }

  goBack() {
    this._location.back();
  }
}
