import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NominiDetailPage } from './nomini-detail.page';

describe('NominiDetailPage', () => {
  let component: NominiDetailPage;
  let fixture: ComponentFixture<NominiDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NominiDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NominiDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
