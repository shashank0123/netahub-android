import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NominiDetailPageRoutingModule } from './nomini-detail-routing.module';

import { NominiDetailPage } from './nomini-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NominiDetailPageRoutingModule
  ],
  declarations: [NominiDetailPage]
})
export class NominiDetailPageModule {}
