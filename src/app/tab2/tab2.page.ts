import { Component, OnInit, HostListener, ViewChild, ElementRef, QueryList, ViewChildren   } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import  { Router   } from '@angular/router';
import { Storage } from '@ionic/storage';
import { environment } from '../../environments/environment';
import { ToastController, Platform, AlertController } from '@ionic/angular';
import { HttpClient } from '@angular/common/http';
import { PopoverController } from '@ionic/angular';
import { SettingsComponent } from '../setting/setting.component';
import { Location } from '@angular/common';
import { Chart } from 'chart.js';
import { ChartDataSets } from 'chart.js';
import { Color, Label } from 'ng2-charts';
import {ChartboardComponent} from '../chartboard/chartboard.component'




@Component({
	selector: 'app-tab2',
	templateUrl: 'tab2.page.html',
	styleUrls: ['tab2.page.scss']
})


export class Tab2Page {

public user: object = {};
profilepic = '';
// @ViewChild("pr_chart1", {static: false}) barCanvas: ElementRef;
// @ViewChild('barChart', {static:false}) barChart;
 @ViewChildren(ChartboardComponent) viewChildren!: QueryList<ChartboardComponent>;
	

  bars: any;
  colorArray: any;
	
  

 
	public widgets=[];
	
	baseUrl: string = environment.url;
	// charts: Chart[] = [];
	token = '';
	user_photo = '';
	constituency = '';
	electiondata: any = '';
	
	constructor(private http: HttpClient,
		private router: Router,
		private storage: Storage,
		private toastController: ToastController,
		private activatedRoute: ActivatedRoute,
		private popoverController: PopoverController,
		private _location:Location) { }

	ionViewDidEnter() {
		
		this.getSessionData();
	}

	

	getSessionData() {


	this.storage.get('user').then((name) => {
      this.user = name;
      this.profilepic =this.user[0].photo_path

      if (!name) {
        this.router.navigate(['/login']);
      }
    });



		this.storage.get('token').then((name) => {
			this.token = name;
			console.log(name)

			this.getElectionData();
			if (!name) {
				this.router.navigateByUrl('/login');
			}
		});
		// get user details
	}


	getElectionData() {
		const completeUrl = `${this.baseUrl}/userdashboard?token=${this.token}`;
		console.log(completeUrl);
		this.http.get(completeUrl).subscribe(data => {
			console.log('circle data: ', data);
			if (data[0].message != undefined && data[0].message == 'Invalid Token') {
				this.router.navigateByUrl('/login');
			} else {
				this.electiondata = data;
        let jokes: ChartboardComponent[] = this.viewChildren.toArray();
				
			};
		});
		
		// this.chartElementRefs.map((chartElementRef, index) => {
			//    const config = Object.assign({}, baseConfig, { data: this.widgets[index] });

			//    return new Chart(chartElementRef.nativeElement, config);
			//  });
			//   }
			// // console.log(this.widgets);
			// });
		}

		goBack(){
		  this._location.back();
		  }


	}
