import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProfessionAnalysisPage } from './profession-analysis.page';

const routes: Routes = [
  {
    path: '',
    component: ProfessionAnalysisPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProfessionAnalysisPageRoutingModule {}
