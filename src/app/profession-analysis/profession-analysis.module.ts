import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { ProfessionAnalysisPageRoutingModule } from './profession-analysis-routing.module';

import { ProfessionAnalysisPage } from './profession-analysis.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProfessionAnalysisPageRoutingModule
  ],
  declarations: [ProfessionAnalysisPage]
})
export class ProfessionAnalysisPageModule {}
