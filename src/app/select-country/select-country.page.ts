import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ToastController, Platform, AlertController, LoadingController } from '@ionic/angular';
import { finalize } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Location } from '@angular/common';

@Component({
  selector: 'app-select-country',
  templateUrl: './select-country.page.html',
  styleUrls: ['./select-country.page.scss'],
})
export class SelectCountryPage implements OnInit {
	baseUrl: string = environment.url;
	 country: any;
	 token: string;
	 btntext: string = "Continue";
  authenticationState = new BehaviorSubject(false);
  public requestData: object = {};
  public responseData: object = { country: '', usercountry: ''};



  public user: object = {};
  profilepic: string = "";



  constructor(private activatedRoute: ActivatedRoute, private router: Router,public loadingController: LoadingController,
    private http: HttpClient,
    private storage: Storage,
    private toastController: ToastController,
    private _location:Location
    ) { }

  ngOnInit() {
    this.getcategorydata();
  }

  sendPostRequest(){

  	this.storage.get('token').then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/updateuserprofile?token=${this.token}&country_id=${this.requestData['usercountry']}`;      
      this.http.get(completeUrl).subscribe(data => {
        if (data) {
          this.router.navigateByUrl('/select-state?country_id='+this.requestData['usercountry']);
        } else {
          this.responseData['error_message'] = data['message'];
        }
      })
      if (!name) {
        this.router.navigateByUrl('/login');
      }
    });
      
  }
   
  getcategorydata(){


  	this.storage.get('token').then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/country?token=${this.token}`;
	    this.http.get(completeUrl).subscribe(data => {
	      console.log('circle data: ', data);
	      this.country = data
	      const completeUrl1 = `${this.baseUrl}/usercountry?token=${this.token}`;
	    this.http.get(completeUrl1).subscribe(data1 => {
	      console.log('circle data: ', data1);
	      if (data1[0] != undefined && data1[0].country_id != undefined)
	      this.requestData['usercountry'] = data1[0].country_id
	  console.log(this.requestData['usercountry'])
	    })
	    })

	    
      if (!name) {
        this.router.navigateByUrl('/login');
      }
    });
    
    
  }

  updatebtn(){
  	console.log(this.btntext)
  	if (this.requestData['usercountry']!=""){
  		this.btntext = "Continue";
  	}
  	else
  		this.btntext = "Skip";

  }

  goBack(){
  this._location.back();
  }

}
