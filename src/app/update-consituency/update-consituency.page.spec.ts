import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { UpdateConsituencyPage } from './update-consituency.page';

describe('UpdateConsituencyPage', () => {
  let component: UpdateConsituencyPage;
  let fixture: ComponentFixture<UpdateConsituencyPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateConsituencyPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(UpdateConsituencyPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
