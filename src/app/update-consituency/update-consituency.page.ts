import { ActivatedRoute, Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { Storage } from "@ionic/storage";
import {
  ToastController,
  Platform,
  AlertController,
  LoadingController,
} from "@ionic/angular";
import { finalize } from "rxjs/operators";
import { environment } from "../../environments/environment";
import { tap, catchError } from "rxjs/operators";
import { BehaviorSubject } from "rxjs";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Location } from "@angular/common";

@Component({
  selector: "app-update-consituency",
  templateUrl: "./update-consituency.page.html",
  styleUrls: ["./update-consituency.page.scss"],
})
export class UpdateConsituencyPage implements OnInit {
  baseUrl: string = environment.url;
  constituency: any;
  token: string;
  district_id: string;
  btntext: string = "Submit";
  authenticationState = new BehaviorSubject(false);
  public columns: any;
  public rows: any;

  public widgets = [];
  public requestData: object = {};
  public responseData: object = { state: "", userstate: "" };

  public user: object = {};
  profilepic: string = "";

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public loadingController: LoadingController,
    private http: HttpClient,
    private storage: Storage,
    private toastController: ToastController,
    private _location: Location
  ) {
    this.columns = [{ prop: "name" }, { name: "Summary" }, { name: "Company" }];
  }

  ngOnInit() {
    this.district_id = this.activatedRoute.snapshot.queryParamMap.get(
      "district_id"
    );
    this.getcategorydata();
  }

  updateall() {
    this.widgets.forEach((newobj) => {
      var completeUrl = `${this.baseUrl}/deleteuserconsmap?token=${this.token}&election_id=${newobj.election_id}&cons_id=${newobj.cons_id}`;
      console.log(completeUrl);
      this.http.get(completeUrl).subscribe((data) => {
        if (data) {
          console.log(data);
          // this.router.navigateByUrl('/select-block?tehsil_id='+this.requestData['usertehsil']);
        } else {
          this.responseData["error_message"] = data["message"];
        }
      });
    });
    var completeUrl = `${this.baseUrl}/updateuserconsstatus?token=${this.token}`;
    console.log(completeUrl);
    this.http.get(completeUrl).subscribe((data) => {
      if (data) {
        console.log(data);
        this.loadingController
          .create({
            message: "Data Saved Successfully",
            duration: 2000,
          })
          .then((res) => {
            res.present().then(() => {
              this.router.navigateByUrl("tabs/tab1");
            });
          });
        // this.router.navigateByUrl('/select-block?tehsil_id='+this.requestData['usertehsil']);
      } else {
        this.responseData["error_message"] = data[0]["message"];
        this.loadingController
          .create({
            message: this.responseData["error_message"],
            duration: 2000,
          })
          .then((res) => {
            res.present().then(() => {
              this.router.navigateByUrl("tabs/tab1");
            });
          });
      }
    });
    this.router.navigateByUrl("/members");
  }

  add(election) {
    let index = this.constituency.indexOf(election);
    if (index > -1) {
      this.constituency.splice(index, 1);
    }
    this.widgets.push({
      election_id: election.election_id,
      cons_id: election.cons_id,
    });
    console.log(this.widgets);
  }

  getcategorydata() {
    this.storage.get("user").then((name) => {
      this.user = name;
      this.profilepic = this.user[0].photo_path;

      if (!name) {
        this.router.navigate(["/login"]);
      }
    });

    this.storage.get("token").then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/userconstituency?token=${this.token}`;
      console.log(completeUrl);
      this.http.get(completeUrl).subscribe((data) => {
        console.log("circle data: ", data);
        this.constituency = data;
        this.rows = data;
      });

      if (!name) {
        this.router.navigateByUrl("/login");
      }
    });
  }

  updatebtn() {
    console.log(this.btntext);
    if (this.requestData["userdistrict"] != "") {
      this.btntext = "Submit";
    } else this.btntext = "Skip";
  }

  goBack() {
    this._location.back();
  }
}
