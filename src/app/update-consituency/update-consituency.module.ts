import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { UpdateConsituencyPageRoutingModule } from './update-consituency-routing.module';

import { UpdateConsituencyPage } from './update-consituency.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    UpdateConsituencyPageRoutingModule
  ],
  declarations: [UpdateConsituencyPage]
})
export class UpdateConsituencyPageModule {}
