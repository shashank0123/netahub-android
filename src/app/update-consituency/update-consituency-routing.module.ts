import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { UpdateConsituencyPage } from './update-consituency.page';

const routes: Routes = [
  {
    path: '',
    component: UpdateConsituencyPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class UpdateConsituencyPageRoutingModule {}
