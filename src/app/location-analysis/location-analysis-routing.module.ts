import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { LocationAnalysisPage } from './location-analysis.page';

const routes: Routes = [
  {
    path: '',
    component: LocationAnalysisPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class LocationAnalysisPageRoutingModule {}
