import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ToastController, Platform, AlertController, LoadingController } from '@ionic/angular';
import { finalize } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Location } from '@angular/common';

@Component({
  selector: 'app-location-analysis',
  templateUrl: './location-analysis.page.html',
  styleUrls: ['./location-analysis.page.scss'],
})
export class LocationAnalysisPage implements OnInit {
	survey_list:any
	analysisreport:any
	nominee_list:any
	survey_id:any
	nominee_id:any
	location_type:any
	baseUrl: string = environment.url;
	 country: any;
	 token: string;
	 btntext: string = "Continue";
  authenticationState = new BehaviorSubject(false);
  public requestData: object = {};
  public responseData: object = { country: '', usercountry: ''};

  public user: object = {};
  profilepic: string = "";

  constructor(private activatedRoute: ActivatedRoute, private router: Router,public loadingController: LoadingController,
    private http: HttpClient,
    private storage: Storage,
    private toastController: ToastController,
    private _location:Location) { }

  ngOnInit() {
  	this.location_type = this.activatedRoute.snapshot.queryParamMap.get("location-type")
    this.getcategorydata();
  }

  getcategorydata(){


   this.storage.get('user').then((name) => {
      this.user = name;
      this.profilepic =this.user[0].photo_path

      if (!name) {
        this.router.navigate(['/login']);
      }
    });
    

  	this.storage.get('token').then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/reportsurvey?token=${this.token}&location_type=${this.location_type}`;
	    this.http.get(completeUrl).subscribe(data => {
	      this.survey_list = data
	    })

	    
      if (!name) {
        this.router.navigateByUrl('/login');
      }
    });
    
    
  }

  updatebtn(){
    this.nominee_list = []
    this.nominee_id = ''
  	const completeUrl = `${this.baseUrl}/reportsurveynominee?token=${this.token}&survey_id=${this.survey_id}`;
  	console.log('hi');
	    this.http.get(completeUrl).subscribe(data => {
	      this.nominee_list = data
	    })

  }

  updateList(){
  	const completeUrl = `${this.baseUrl}/analysislocation?token=${this.token}&survey_id=${this.survey_id}&nominee_id=${this.nominee_id}&location_type=${this.location_type}`;
  	console.log('hi');
	    this.http.get(completeUrl).subscribe(data => {
	      console.log('circle data6: ', data);
	      this.analysisreport = data
	    })

  }

  goBack(){
  this._location.back();
  }

}
