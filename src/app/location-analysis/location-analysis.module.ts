import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { LocationAnalysisPageRoutingModule } from './location-analysis-routing.module';

import { LocationAnalysisPage } from './location-analysis.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    LocationAnalysisPageRoutingModule
  ],
  declarations: [LocationAnalysisPage]
})
export class LocationAnalysisPageModule {}
