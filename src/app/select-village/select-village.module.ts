import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectVillagePageRoutingModule } from './select-village-routing.module';

import { SelectVillagePage } from './select-village.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectVillagePageRoutingModule
  ],
  declarations: [SelectVillagePage]
})
export class SelectVillagePageModule {}
