import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectVillagePage } from './select-village.page';

describe('SelectVillagePage', () => {
  let component: SelectVillagePage;
  let fixture: ComponentFixture<SelectVillagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectVillagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectVillagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
