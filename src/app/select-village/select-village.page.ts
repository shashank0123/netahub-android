import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ToastController, Platform, AlertController, LoadingController } from '@ionic/angular';
import { finalize } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Location } from '@angular/common';

@Component({
  selector: 'app-select-village',
  templateUrl: './select-village.page.html',
  styleUrls: ['./select-village.page.scss'],
})
export class SelectVillagePage implements OnInit {
	baseUrl: string = environment.url;
	 village: any;
	 token: string;
	 block_id: string;
	 // pincode: string;
	 btntext: string = "Continue";
  authenticationState = new BehaviorSubject(false);
  public requestData: object = {};
  public responseData: object = { state: '', userstate: ''};

  constructor(private activatedRoute: ActivatedRoute, private router: Router,public loadingController: LoadingController,
    private http: HttpClient,
    private storage: Storage,
    private toastController: ToastController,
    private _location:Location
    ) { }

  ngOnInit() {
  	this.block_id = this.activatedRoute.snapshot.queryParamMap.get("block_id")
    this.getcategorydata();
  }

  sendPostRequest(){

  	this.storage.get('token').then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/updateuserprofile?token=${this.token}&village_id=${this.requestData['uservillage']}&pincode=${this.requestData['userpincode']}&block_id=${this.block_id}&tehsil_name=${this.requestData['uservillagename']}`;      
      this.http.get(completeUrl).subscribe(data => {
        if (data) {
          this.loadingController.create({
                message: 'Data Saved Successfully',
                duration: 2000
              }).then((res) => {
                res.present();
                this.router.navigateByUrl('/select-ward?village_id='+this.requestData['uservillage']);
              });
          
        } else {
          this.responseData['error_message'] = data['message'];
        }
      })
      if (!name) {
        this.router.navigateByUrl('/login');
      }
    });
      
  }
   
  getcategorydata(){


  	this.storage.get('token').then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/village?token=${this.token}&block_id=${this.block_id}`;
	    this.http.get(completeUrl).subscribe(data => {
	      console.log('circle data: ', data);
	      this.village = data
	      const completeUrl1 = `${this.baseUrl}/uservillage?token=${this.token}`;
	    this.http.get(completeUrl1).subscribe(data1 => {
	      console.log('circle data: ', data1);
	      if (data1[0] != undefined && data1[0].village_id != undefined)
	      this.requestData['uservillage'] = data1[0].village_id
	  console.log(this.requestData['uservillage'])
	    })
	    })
	  const completeUrl2 = `${this.baseUrl}/user?token=${this.token}`;
      console.log(completeUrl2);
      this.http.get(completeUrl2).subscribe(data2 => {    
        if (data2[0]!=undefined) {
          console.log(data2);
          this.storage.set('user', data2);
          this.requestData['userpincode'] =data2[0].pincode
        } else {
        }
      })

	    
      if (!name) {
        this.router.navigateByUrl('/login');
      }
    });
    
    
  }

  updatebtn(){
  	console.log(this.btntext)
  	if (this.requestData['uservillage']!=""){
  		this.btntext = "Continue";
  	}
  	else
  		this.btntext = "Skip";

  }

  goBack(){
  this._location.back();
  }

}
