import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectVillagePage } from './select-village.page';

const routes: Routes = [
  {
    path: '',
    component: SelectVillagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectVillagePageRoutingModule {}
