import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ActivatedRoute } from '@angular/router';
import { ToastController, Platform, AlertController, LoadingController } from '@ionic/angular';
import { finalize } from 'rxjs/operators';
import { Router } from '@angular/router';
import { environment } from '../../environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import {
  Plugins,
  PushNotification,
  PushNotificationToken,
  PushNotificationActionPerformed } from '@capacitor/core';

const { PushNotifications } = Plugins;

@Component({
  selector: 'app-login',
  templateUrl: './login.page.html',
  styleUrls: ['./login.page.scss'],
})
 
export class LoginPage implements OnInit {
  baseUrl: string = environment.url;
  authenticationState = new BehaviorSubject(false);
  public requestData: object = {};
  public responseData: object = { username: '9560755823', password: 'vishal@123' };
  passwordType: string = 'password';
  passwordIcon: string = 'eye-off';
  firebase_token: string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router,public loadingController: LoadingController,
    private http: HttpClient,
    private storage: Storage,
    private toastController: ToastController) { }
  

  ngOnInit() {
    this.storage.set('token', null);
    // PushNotifications.initializeApp(this);
        // Register with Apple / Google to receive push via APNS/FCM
    PushNotifications.register();
    

    // On success, we should be able to receive notifications
    PushNotifications.addListener('registration',
      (token: PushNotificationToken) => {
        this.firebase_token = token.value
        // alert('Push registration success, token: ' + token.value);
      }
    );

    // Some issue with our setup and push will not work
    PushNotifications.addListener('registrationError',
      (error: any) => {
        // alert('Error on registration: ' + JSON.stringify(error));
      }
    );

    // Show us the notification payload if the app is open on our device
    PushNotifications.addListener('pushNotificationReceived',
      (notification: PushNotification) => {
        // alert('Push received: ' + JSON.stringify(notification));
      }
    );

    // Method called when tapping on a notification
    PushNotifications.addListener('pushNotificationActionPerformed',
      (notification: PushNotificationActionPerformed) => {
        // alert('Push action performed: ' + JSON.stringify(notification));
      }
    );

  }

  onSubmit(){
    // console.log(this.requestData);
    this.storage.set('token', null);
    this.authenticationState.next(false);
    this.storage.set('token', "");
    const httpOptions = {headers:{'Content-Type': 'application/json', 'Access-Control-Allow-Origin': 'get'}};
    if (this.requestData['login'] && this.requestData['password']) {
      this.showLoader();
      const completeUrl = `${this.baseUrl}/login?login=${this.requestData['login']}&password=${this.requestData['password']}&device_token=${this.firebase_token}&imei=dfasjkdhfsla&model=redme`;      
      this.http.get(completeUrl).subscribe(data => {
        this.hideLoader();
        console.log(data[0].token);
        if (data) {
          console.log(data)
          if (data[0].message != undefined && data[0].message == "Invalid Login or Password"){
            this.responseData['error_message'] = "Login Invalid";
            this.loadingController.create({
                message: 'Invalid Login',
                duration: 2000
              }).then((res) => {
                res.present();
              });
          return 0;
          }

          this.authenticationState.next(true);
          this.storage.set('token', data[0].token);
          const completeUrl1 = `${this.baseUrl}/user?token=${data[0].token}&login_id=${this.requestData['login']}`;
      
            this.http.get(completeUrl1).subscribe(data1 => {

              console.log(data1)
              if (data1) {
                this.storage.set('user', data1);
                this.storage.set('user_photo', data1[0].photo_path);
                this.storage.set('constituency', data1[0].val_cons);
                
              } else {
                this.responseData['error_message'] = data['message'];
              }
            })
            // .then(() => {
            // })
          
        } else {
          this.responseData['error_message'] = data['message'];
        }
      })
    } else {
      this.responseData['error_message'] = "invalid mobile number or password";
    }
            this.router.navigateByUrl('');
  }

  getRegister(){
  this.router.navigateByUrl('register');
  }

  getForgot(){
  this.router.navigateByUrl('forgotpassword');
  }

  showLoader() {
    this.loadingController.create({
      message: 'Please wait...',
      // duration: 2000
    }).then((res) => {
      res.present();
    });
  }
  //
  hideLoader() {
    this.loadingController.dismiss();
  }
  

  getHeaders(header?): any {
    const headers = new HttpHeaders();
    headers.append('Access-Control-Allow-Origin', '*');
    return headers;
  }

}
