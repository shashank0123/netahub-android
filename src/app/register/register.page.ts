import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ToastController, Platform, AlertController, LoadingController } from '@ionic/angular';
import { finalize } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  baseUrl: string = environment.url;
  authenticationState = new BehaviorSubject(false);
  public requestData: object = {};
  public responseData: object = { username: '9560755823', password: 'vishal@123', gender:"", email:"", dob:"" , name:"" };
  dateofbirth :string;

  constructor(private activatedRoute: ActivatedRoute, private router: Router,public loadingController: LoadingController,
    private http: HttpClient,
    private storage: Storage,
    private toastController: ToastController) { }

  ngOnInit() {
  }


  sendPostRequest(){
    const httpOptions = {headers:{'Content-Type': 'application/json', 'Access-Control-Allow-Origin': 'get'}};
    if (this.requestData['name'] && this.requestData['password']) {
      this.dateofbirth =  this.requestData['dob'].split('T')[0]
      this.showLoader();
      const completeUrl = `${this.baseUrl}/insertuser?display_name=${this.requestData['name']}&user_password=${this.requestData['password']}&email_id=${this.requestData['email']}&dateofbirth=${this.dateofbirth}&mobile_no=${this.requestData['mobile']}&gender=${this.requestData['gender']}&qualification=${this.requestData['qualification']}&status=A&occupation=${this.requestData['occupation']}&referred_by=${this.requestData['referred_by']}`;
      // console.log(completeUrl)
      
      this.http.get(completeUrl).subscribe(data => {
        this.hideLoader();
        // console.log(data);
        if (data) {
          this.authenticationState.next(true);
          this.storage.set('user', this.requestData);
          this.storage.set('token', data[0].token);
          this.router.navigateByUrl('home');
        } else {
          this.responseData['error_message'] = data['message'];
        }
      })
    } else {
      this.responseData['error_message'] = "invalid mobile number or password";
    }
  this.router.navigateByUrl('login');
  }

  getLogin(){
  this.router.navigateByUrl('login');
  }

  showLoader() {
    this.loadingController.create({
      message: 'Please wait...',
      // duration: 2000
    }).then((res) => {
      res.present();
    });
  }
  //
  hideLoader() {
    this.loadingController.dismiss();
  }

}
