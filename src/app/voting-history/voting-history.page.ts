import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import  { Router   } from '@angular/router';

@Component({
  selector: 'app-voting-history',
  templateUrl: './voting-history.page.html',
  styleUrls: ['./voting-history.page.scss'],
})
export class VotingHistoryPage implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }

getElections(){
   this.router.navigate(['home']);
  }

  getProfile(){
   this.router.navigate(['profile']);
  }

}
