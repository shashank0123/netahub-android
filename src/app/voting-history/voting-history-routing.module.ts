import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { VotingHistoryPage } from './voting-history.page';

const routes: Routes = [
  {
    path: '',
    component: VotingHistoryPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class VotingHistoryPageRoutingModule {}
