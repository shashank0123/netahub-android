import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VotingHistoryPageRoutingModule } from './voting-history-routing.module';

import { VotingHistoryPage } from './voting-history.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VotingHistoryPageRoutingModule
  ],
  declarations: [VotingHistoryPage]
})
export class VotingHistoryPageModule {}
