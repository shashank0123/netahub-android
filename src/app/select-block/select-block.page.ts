import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ToastController, Platform, AlertController, LoadingController } from '@ionic/angular';
import { finalize } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Location } from '@angular/common';

@Component({
  selector: 'app-select-block',
  templateUrl: './select-block.page.html',
  styleUrls: ['./select-block.page.scss'],
})
export class SelectBlockPage implements OnInit {
	baseUrl: string = environment.url;
	 block: any;
	 token: string;
	 tehsil_id: string;
	 btntext: string = "Continue";
  authenticationState = new BehaviorSubject(false);
  public requestData: object = {};
  public responseData: object = { state: '', userstate: ''};



  public user: object = {};
  profilepic: string = "";
  
  

  constructor(private activatedRoute: ActivatedRoute, private router: Router,public loadingController: LoadingController,
    private http: HttpClient,
    private storage: Storage,
    private toastController: ToastController,
    private _location:Location
    ) { }

  ngOnInit() {
  	this.tehsil_id = this.activatedRoute.snapshot.queryParamMap.get("tehsil_id")
    this.getcategorydata();
  }

  sendPostRequest(){

  	this.storage.get('token').then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/updateuserprofile?token=${this.token}&block_id=${this.requestData['userblock']}&tehsil_id=${this.tehsil_id}&tehsil_name=${this.requestData['userblockname']}`;      
      this.http.get(completeUrl).subscribe(data => {
        if (data) {
          this.router.navigateByUrl('/select-village?block_id='+this.requestData['userblock']);
        } else {
          this.responseData['error_message'] = data['message'];
        }
      })
      if (!name) {
        this.router.navigateByUrl('/login');
      }
    });
      
  }
   
  getcategorydata(){


  	this.storage.get('token').then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/block?token=${this.token}&tehsil_id=${this.tehsil_id}`;
	    this.http.get(completeUrl).subscribe(data => {
	      console.log('circle data: ', data);
	      this.block = data
	      const completeUrl1 = `${this.baseUrl}/userblock?token=${this.token}`;
	    this.http.get(completeUrl1).subscribe(data1 => {
	      console.log('circle data: ', data1);
	      if (data1[0] != undefined && data1[0].block_id != undefined)
	      this.requestData['userblock'] = data1[0].block_id
	  console.log(this.requestData['userblock'])
	    })
	    })

	    
      if (!name) {
        this.router.navigateByUrl('/login');
      }
    });
    
    
  }

  updatebtn(){
  	console.log(this.btntext)
  	if (this.requestData['userdistrict']!=""){
  		this.btntext = "Continue";
  	}
  	else
  		this.btntext = "Skip";

  }

  goBack(){
  this._location.back();
  }

}
