import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectBlockPage } from './select-block.page';

describe('SelectBlockPage', () => {
  let component: SelectBlockPage;
  let fixture: ComponentFixture<SelectBlockPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectBlockPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectBlockPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
