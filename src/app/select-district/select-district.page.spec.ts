import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectDistrictPage } from './select-district.page';

describe('SelectDistrictPage', () => {
  let component: SelectDistrictPage;
  let fixture: ComponentFixture<SelectDistrictPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectDistrictPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectDistrictPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
