import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectDistrictPageRoutingModule } from './select-district-routing.module';

import { SelectDistrictPage } from './select-district.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectDistrictPageRoutingModule
  ],
  declarations: [SelectDistrictPage]
})
export class SelectDistrictPageModule {}
