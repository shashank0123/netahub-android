import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ToastController, Platform, AlertController, LoadingController } from '@ionic/angular';
import { finalize } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Location } from '@angular/common';

@Component({
  selector: 'app-select-district',
  templateUrl: './select-district.page.html',
  styleUrls: ['./select-district.page.scss'],
})
export class SelectDistrictPage implements OnInit {
	baseUrl: string = environment.url;
	 district: any;
	 token: string;
	 new_element: string = "0";
   state_id: string;
	 btntext: string = "Continue";
  authenticationState = new BehaviorSubject(false);
  public requestData: object = {};
  public responseData: object = { state: '', userstate: ''};



  public user: object = {};
  profilepic: string = "";
    


  constructor(private activatedRoute: ActivatedRoute, private router: Router,public loadingController: LoadingController,
    private http: HttpClient,
    private storage: Storage,
    private toastController: ToastController,
    private _location:Location
    ) { }

  ngOnInit() {
  	this.state_id = this.activatedRoute.snapshot.queryParamMap.get("state_id")
    this.getcategorydata();
  }

  sendPostRequest(){

  	this.storage.get('token').then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/updateuserprofile?token=${this.token}&district_id=${this.requestData['userdistrict']}&state_id=${this.state_id}&district_name=${this.requestData['userdistrictname']}`;      
      this.http.get(completeUrl).subscribe(data => {
        if (data) {
          this.router.navigateByUrl('/select-tehsil?district_id='+this.requestData['userdistrict']);
        } else {
          this.responseData['error_message'] = data['message'];
        }
      })
      if (!name) {
        this.router.navigateByUrl('/login');
      }
    });
      
  }
   
  getcategorydata(){


  	this.storage.get('token').then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/district?token=${this.token}&state_id=${this.state_id}`;
	    this.http.get(completeUrl).subscribe(data => {
	      console.log('circle data: ', data);
	      this.district = data
	      const completeUrl1 = `${this.baseUrl}/userdistrict?token=${this.token}`;
	    this.http.get(completeUrl1).subscribe(data1 => {
	      console.log('circle data: ', data1);
	      if (data1[0] != undefined && data1[0].district_id != undefined)
	      this.requestData['userdistrict'] = data1[0].district_id
	  console.log(this.requestData['usercountry'])
	    })
	    })

	    
      if (!name) {
        this.router.navigateByUrl('/login');
      }
    });
    
    
  }

  updatebtn(){
  	console.log(this.btntext)
  	if (this.requestData['userdistrict']!=""){
  		this.btntext = "Continue";
  	}
  	else
  		this.btntext = "Skip";

  }

  goBack(){
  this._location.back();
  }

}
