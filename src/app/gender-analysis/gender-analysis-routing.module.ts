import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { GenderAnalysisPage } from './gender-analysis.page';

const routes: Routes = [
  {
    path: '',
    component: GenderAnalysisPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class GenderAnalysisPageRoutingModule {}
