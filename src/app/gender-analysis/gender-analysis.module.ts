import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { GenderAnalysisPageRoutingModule } from './gender-analysis-routing.module';

import { GenderAnalysisPage } from './gender-analysis.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    GenderAnalysisPageRoutingModule
  ],
  declarations: [GenderAnalysisPage]
})
export class GenderAnalysisPageModule {}
