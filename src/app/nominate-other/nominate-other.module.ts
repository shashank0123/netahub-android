import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { NominateOtherPageRoutingModule } from './nominate-other-routing.module';

import { NominateOtherPage } from './nominate-other.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    NominateOtherPageRoutingModule
  ],
  declarations: [NominateOtherPage]
})
export class NominateOtherPageModule {}
