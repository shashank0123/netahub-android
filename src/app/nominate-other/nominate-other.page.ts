import { Component, OnInit, HostListener } from "@angular/core";
import { ActivatedRoute } from "@angular/router";
import { Router } from "@angular/router";
import { Storage } from "@ionic/storage";
import { environment } from "../../environments/environment";
import {
  ToastController,
  Platform,
  AlertController,
  LoadingController,
} from "@ionic/angular";
import { HttpClient } from "@angular/common/http";

@Component({
  selector: "app-nominate-other",
  templateUrl: "./nominate-other.page.html",
  styleUrls: ["./nominate-other.page.scss"],
})
export class NominateOtherPage implements OnInit {
  categories: any;
  party_id: any;
  profilepic: any;
  token: any;
  survey_id: any;
  cons_id: any;
  public requestData: object = {};
  baseUrl: string = environment.url;

  public user: object = {};

  constructor(
    private http: HttpClient,
    private router: Router,
    private storage: Storage,
    private toastController: ToastController,
    private activatedRoute: ActivatedRoute,
    private loadingController: LoadingController
  ) {}

  ngOnInit() {
    this.token = this.activatedRoute.snapshot.queryParamMap.get("token");
    this.survey_id = this.activatedRoute.snapshot.queryParamMap.get(
      "survey_id"
    );
    this.cons_id = this.activatedRoute.snapshot.queryParamMap.get("cons_id");
    this.getSessionData();
  }
  getSessionData() {
    this.storage.get("user").then((name) => {
      this.user = name;
      this.profilepic = this.user[0].photo_path;

      if (!name) {
        this.router.navigate(["/login"]);
      }
    });

    this.getcategorydata();
  }

  getcategorydata() {
    const completeUrl = `${this.baseUrl}/party?token=${this.token}`;
    this.http.get(completeUrl).subscribe((data) => {
      console.log("circle data: ", data);
      this.categories = data;
    });
  }
  // 'nominee_name', 'photo_path', 'nominee_desc'

  sendPostRequest() {
    console.log(this.party_id);
    const completeUrl = `${this.baseUrl}/insertnominee?token=${this.token}&survey_id=${this.survey_id}&party_id=&cons_id=${this.cons_id}&nominee_name=${this.requestData["name"]}&photo_path=&nominee_desc=${this.requestData["nominee_desc"]}&mobile_no=${this.requestData["mobile_no"]}`;
    this.http.get(completeUrl).subscribe((data) => {
      console.log("circle data: ", data);
      this.loadingController
        .create({
          message: "Nominee Created successfully",
          duration: 2000,
        })
        .then((res) => {
          res.present().then(() => {
            this.router.navigateByUrl("tabs/tab1");
          });
        });
      // this.nomineeData = data;
    });
  }
}
