import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { NominateOtherPage } from './nominate-other.page';

describe('NominateOtherPage', () => {
  let component: NominateOtherPage;
  let fixture: ComponentFixture<NominateOtherPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ NominateOtherPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(NominateOtherPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
