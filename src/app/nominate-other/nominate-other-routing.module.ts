import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { NominateOtherPage } from './nominate-other.page';

const routes: Routes = [
  {
    path: '',
    component: NominateOtherPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class NominateOtherPageRoutingModule {}
