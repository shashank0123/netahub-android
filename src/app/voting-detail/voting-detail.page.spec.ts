import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { VotingDetailPage } from './voting-detail.page';

describe('VotingDetailPage', () => {
  let component: VotingDetailPage;
  let fixture: ComponentFixture<VotingDetailPage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ VotingDetailPage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(VotingDetailPage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
