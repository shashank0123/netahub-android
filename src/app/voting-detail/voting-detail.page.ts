import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import  { Router   } from '@angular/router';

@Component({
  selector: 'app-voting-detail',
  templateUrl: './voting-detail.page.html',
  styleUrls: ['./voting-detail.page.scss'],
})
export class VotingDetailPage implements OnInit {

  constructor(private activatedRoute: ActivatedRoute, private router: Router) { }

  ngOnInit() {
  }

getElections(){
   this.router.navigate(['home']);
  }

  getProfile(){
   this.router.navigate(['profile']);
  }

}
