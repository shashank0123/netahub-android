import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { VotingDetailPageRoutingModule } from './voting-detail-routing.module';

import { VotingDetailPage } from './voting-detail.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    VotingDetailPageRoutingModule
  ],
  declarations: [VotingDetailPage]
})
export class VotingDetailPageModule {}
