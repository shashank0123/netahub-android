import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { AgeAnalysisPageRoutingModule } from './age-analysis-routing.module';

import { AgeAnalysisPage } from './age-analysis.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    AgeAnalysisPageRoutingModule
  ],
  declarations: [AgeAnalysisPage]
})
export class AgeAnalysisPageModule {}
