import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { AgeAnalysisPage } from './age-analysis.page';

const routes: Routes = [
  {
    path: '',
    component: AgeAnalysisPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class AgeAnalysisPageRoutingModule {}
