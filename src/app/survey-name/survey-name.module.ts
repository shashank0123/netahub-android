import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SurveyNamePageRoutingModule } from './survey-name-routing.module';

import { SurveyNamePage } from './survey-name.page';
import { DatePipe } from '@angular/common';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SurveyNamePageRoutingModule
  ],
  providers: [
    DatePipe,
  ],
  declarations: [SurveyNamePage]
})
export class SurveyNamePageModule {}
