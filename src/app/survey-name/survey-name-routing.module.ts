import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SurveyNamePage } from './survey-name.page';

const routes: Routes = [
  {
    path: '',
    component: SurveyNamePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SurveyNamePageRoutingModule {}
