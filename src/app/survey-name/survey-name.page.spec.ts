import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SurveyNamePage } from './survey-name.page';

describe('SurveyNamePage', () => {
  let component: SurveyNamePage;
  let fixture: ComponentFixture<SurveyNamePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SurveyNamePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SurveyNamePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
