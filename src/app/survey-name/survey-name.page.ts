import { ActivatedRoute, Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { Storage } from "@ionic/storage";
import {
  ToastController,
  Platform,
  AlertController,
  LoadingController,
} from "@ionic/angular";
import { finalize } from "rxjs/operators";
import { environment } from "../../environments/environment";
import { tap, catchError } from "rxjs/operators";
import { BehaviorSubject } from "rxjs";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { DatePipe } from "@angular/common";

@Component({
  selector: "app-survey-name",
  templateUrl: "./survey-name.page.html",
  styleUrls: ["./survey-name.page.scss"],
})
export class SurveyNamePage implements OnInit {
  baseUrl: string = environment.url;
  token: string;
  btntext: string = "Create";
  election_id: string;
  election_name: string;
  public requestData: object = {};
  // myDate:any
  myDate1: any;
  myDate: any;
  newdate: any;
  name: any;
  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public loadingController: LoadingController,
    private http: HttpClient,
    private storage: Storage,
    private toastController: ToastController,
    private datePipe: DatePipe
  ) {
    // this.myDate = this.datePipe.transform(this.myDate, 'yyyy-MM-dd');
    this.myDate1 = new Date();
    var numberOfDaysToAdd = 10;
    this.myDate = this.datePipe.transform(
      this.myDate1.setDate(this.myDate1.getDate()),
      "yyyy-MM-dd"
    );
    this.newdate = this.datePipe.transform(
      this.myDate1.setDate(this.myDate1.getDate() + numberOfDaysToAdd),
      "yyyy-MM-dd"
    );
  }

  ngOnInit() {
    this.election_id = this.activatedRoute.snapshot.queryParamMap.get(
      "election_id"
    );
    this.election_name = this.activatedRoute.snapshot.queryParamMap.get(
      "election_name"
    );
    this.token = this.activatedRoute.snapshot.queryParamMap.get("token");
  }

  sendPostRequest() {
    const completeUrl = `${this.baseUrl}/insertsurvey?token=${this.token}&election_id=${this.election_id}&survey_name=${this.requestData["survey"]}&from_date=${this.myDate}&to_date=${this.newdate}`;
    console.log(completeUrl);
    this.http.get(completeUrl).subscribe((data) => {
      if (data) {
        console.log(data);
        this.name = data[0].message;
        this.loadingController
          .create({
            message: "Survey Created successfully",
            duration: 2000,
          })
          .then((res) => {
            res.present().then(() => {
              this.router.navigateByUrl("tabs/tab1");
            });
          });
        // this.router.navigateByUrl('/select-society?ward_id='+this.requestData['userward']);
        // this.router.navigateByUrl('select-ward?village_id='+this.village_id);
        this.router.navigateByUrl("/tabs");
      }
    });
  }
}
