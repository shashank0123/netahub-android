import { ActivatedRoute, Router } from "@angular/router";
import { Component, OnInit } from "@angular/core";
import { Storage } from "@ionic/storage";
import {
  ToastController,
  Platform,
  AlertController,
  LoadingController,
} from "@ionic/angular";
import { finalize } from "rxjs/operators";
import { environment } from "../../environments/environment";
import { tap, catchError } from "rxjs/operators";
import { BehaviorSubject } from "rxjs";
import { HttpClient, HttpHeaders, HttpParams } from "@angular/common/http";
import { Location } from "@angular/common";

@Component({
  selector: "app-select-society",
  templateUrl: "./select-society.page.html",
  styleUrls: ["./select-society.page.scss"],
})
export class SelectSocietyPage implements OnInit {
  baseUrl: string = environment.url;
  society: any;
  token: string;
  district_id: string;
  btntext: string = "Submit";
  authenticationState = new BehaviorSubject(false);
  public requestData: object = {};
  public responseData: object = { state: "", userstate: "" };

  constructor(
    private activatedRoute: ActivatedRoute,
    private router: Router,
    public loadingController: LoadingController,
    private http: HttpClient,
    private storage: Storage,
    private toastController: ToastController,
    private _location: Location
  ) {}

  ngOnInit() {
    this.district_id = this.activatedRoute.snapshot.queryParamMap.get(
      "district_id"
    );
    this.getcategorydata();
  }

  sendPostRequest() {
    this.storage.get("token").then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/updateuserprofile?token=${this.token}&society_id=${this.requestData["usersociety"]}&district_id=${this.district_id}&society_name=${this.requestData["usersocietyname"]}`;
      this.http.get(completeUrl).subscribe((data) => {
        if (data) {
          this.loadingController
            .create({
              message: "Data Saved Successfully",
              duration: 2000,
            })
            .then((res) => {
              res.present().then(() => {
                this.router.navigateByUrl("profile");
              });
            });
        } else {
          this.responseData["error_message"] = data[0].message;
        }
      });
      if (!name) {
        this.router.navigateByUrl("/login");
      }
    });
  }

  getcategorydata() {
    this.storage.get("token").then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/society?token=${this.token}&district_id=${this.district_id}`;
      console.log(completeUrl);
      this.http.get(completeUrl).subscribe((data) => {
        console.log("circle data: ", data);
        this.society = data;
        const completeUrl1 = `${this.baseUrl}/usersociety?token=${this.token}`;
        this.http.get(completeUrl1).subscribe((data1) => {
          console.log("circle data: ", data1);
          if (data1[0] != undefined && data1[0].society_id != undefined)
            this.requestData["usersociety"] = data1[0].society_id;
          console.log(this.requestData["usersociety"]);
        });
      });

      if (!name) {
        this.router.navigateByUrl("/login");
      }
    });
  }

  updatebtn() {
    console.log(this.btntext);
    if (this.requestData["userdistrict"] != "") {
      this.btntext = "Submit";
    } else this.btntext = "Skip";
  }

  goBack() {
    this._location.back();
  }
}
