import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { SelectStatePage } from './select-state.page';

const routes: Routes = [
  {
    path: '',
    component: SelectStatePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class SelectStatePageRoutingModule {}
