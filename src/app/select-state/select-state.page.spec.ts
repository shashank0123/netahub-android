import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { SelectStatePage } from './select-state.page';

describe('SelectStatePage', () => {
  let component: SelectStatePage;
  let fixture: ComponentFixture<SelectStatePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SelectStatePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(SelectStatePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
