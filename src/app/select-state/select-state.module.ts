import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { SelectStatePageRoutingModule } from './select-state-routing.module';

import { SelectStatePage } from './select-state.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    SelectStatePageRoutingModule
  ],
  declarations: [SelectStatePage]
})
export class SelectStatePageModule {}
