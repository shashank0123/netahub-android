
import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ToastController, Platform, AlertController, LoadingController } from '@ionic/angular';
import { finalize } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Location } from '@angular/common';

@Component({
  selector: 'app-select-state',
  templateUrl: './select-state.page.html',
  styleUrls: ['./select-state.page.scss'],
})
export class SelectStatePage implements OnInit {
	baseUrl: string = environment.url;
	 state: any;
	 token: string;
	 country_id: string;
	 btntext: string = "Continue";
  authenticationState = new BehaviorSubject(false);
  public requestData: object = {};
  public responseData: object = { state: '', userstate: ''};

  constructor(private activatedRoute: ActivatedRoute, private router: Router,public loadingController: LoadingController,
    private http: HttpClient,
    private storage: Storage,
    private toastController: ToastController,
    private _location:Location
    ) { }

  ngOnInit() {
  	this.country_id = this.activatedRoute.snapshot.queryParamMap.get("country_id")
    this.getcategorydata();
  }

  sendPostRequest(){

  	this.storage.get('token').then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/updateuserprofile?token=${this.token}&state_id=${this.requestData['userstate']}`;      
      this.http.get(completeUrl).subscribe(data => {
        if (data) {
          this.router.navigateByUrl('/select-district?state_id='+this.requestData['userstate']);
        } else {
          this.responseData['error_message'] = data['message'];
        }
      })
      if (!name) {
        this.router.navigateByUrl('/login');
      }
    });
      
  }
   
  getcategorydata(){


  	this.storage.get('token').then((name) => {
      this.token = name;
      const completeUrl = `${this.baseUrl}/state?token=${this.token}&country_id=${this.country_id}`;
	    this.http.get(completeUrl).subscribe(data => {
	      console.log('circle data: ', data);
	      this.state = data
	      const completeUrl1 = `${this.baseUrl}/userstate?token=${this.token}`;
	    this.http.get(completeUrl1).subscribe(data1 => {
	      console.log('circle data: ', data1);
	      if (data1[0] != undefined && data1[0].state_id != undefined)
	      this.requestData['userstate'] = data1[0].state_id
	  console.log(this.requestData['userstate'])
	    })
	    })

	    
      if (!name) {
        this.router.navigateByUrl('/login');
      }
    });
    
    
  }

  updatebtn(){
  	console.log(this.btntext)
  	if (this.requestData['usercountry']!=""){
  		this.btntext = "Continue";
  	}
  	else
  		this.btntext = "Skip";

  }

  goBack(){
  this._location.back();
  }

}
