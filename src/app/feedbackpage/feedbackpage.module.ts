import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { FeedbackpagePageRoutingModule } from './feedbackpage-routing.module';

import { FeedbackpagePage } from './feedbackpage.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    FeedbackpagePageRoutingModule
  ],
  declarations: [FeedbackpagePage]
})
export class FeedbackpagePageModule {}
