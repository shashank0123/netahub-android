import { ActivatedRoute, Router } from '@angular/router';
import { Component, OnInit } from '@angular/core';
import { Storage } from '@ionic/storage';
import { ToastController, Platform, AlertController, LoadingController } from '@ionic/angular';
import { finalize } from 'rxjs/operators';
import { environment } from '../../environments/environment';
import { tap, catchError } from 'rxjs/operators';
import { BehaviorSubject } from 'rxjs';
import { HttpClient, HttpHeaders, HttpParams } from '@angular/common/http';
import { Location } from '@angular/common';

@Component({
  selector: 'app-feedbackpage',
  templateUrl: './feedbackpage.page.html',
  styleUrls: ['./feedbackpage.page.scss'],
})
export class FeedbackpagePage implements OnInit {
	baseUrl: string = environment.url;
  authenticationState = new BehaviorSubject(false);
  token:string
  public requestData: object = {};
  public responseData: object = { feedback_desc: 'test'};

  public user: object = {};
  profilepic: string = "";
  


  constructor(private activatedRoute: ActivatedRoute, private router: Router,public loadingController: LoadingController,
    private http: HttpClient,
    private storage: Storage,
    private toastController: ToastController,
    private _location:Location
    ) { }

  ngOnInit() {
  	this.storage.get('token').then((name) => {
      this.token = name;
      if (!name) {
        this.router.navigateByUrl('/login');
      }
    });


this.storage.get('user').then((name) => {
      this.user = name;
      this.profilepic =this.user[0].photo_path

      if (!name) {
        this.router.navigate(['/login']);
      }
    });

    
  }

  sendPostRequest(){
    const httpOptions = {headers:{'Content-Type': 'application/json', 'Access-Control-Allow-Origin': 'get'}};
      // this.showLoader();
      const completeUrl = `${this.baseUrl}/insertfeedback?token=${this.token}&feedback_desc=${this.requestData['feedback_desc']}`;
	  	console.log(completeUrl);
      
      this.http.get(completeUrl).subscribe(data => {
        // this.hideLoader();
        console.log(data);
        if (data) {
        	this.loadingController.create({
                message: data[0].message,
                duration: 2000
              }).then((res) => {
                res.present();
              });

              if (data[0].status== "Success")
              	this.router.navigateByUrl('/');
              
          console.log(data)
        } else {
        }
      })
  }

goBack(){
  this._location.back();
  }
  
}
