import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { FeedbackpagePage } from './feedbackpage.page';

const routes: Routes = [
  {
    path: '',
    component: FeedbackpagePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class FeedbackpagePageRoutingModule {}
