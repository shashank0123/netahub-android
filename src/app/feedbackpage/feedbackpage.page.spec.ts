import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { FeedbackpagePage } from './feedbackpage.page';

describe('FeedbackpagePage', () => {
  let component: FeedbackpagePage;
  let fixture: ComponentFixture<FeedbackpagePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FeedbackpagePage ],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(FeedbackpagePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
